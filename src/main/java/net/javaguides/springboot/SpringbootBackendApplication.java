package net.javaguides.springboot;

import net.javaguides.springboot.model.User;
import net.javaguides.springboot.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootBackendApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootBackendApplication.class, args);
	}

	@Autowired
	private UserRepository userRepository;

	@Override
	public void run(String... args) throws Exception {
//		User user = new User();
//		user.setFirstName("Aldy");
//		user.setEmail("aldyalexandermh@gmail.com");
//		user.setTelephone_no("081321202309");
//		user.setAddress("Nirwana Asri Block C no 13");
//		userRepository.save(user);
//
//		User user1 = new User();
//		user1.setFirstName("Budi");
//		user1.setEmail("budi@gmail.com");
//		user.setTelephone_no("081321202309");
//		user.setAddress("Nirwana Asri Block C no 13");
//		userRepository.save(user1);
	}
}
